'use strict';


var myApp = myApp || {};
var namespace = myApp.storage = myApp.storage || {};



(function (window, namespace) {


	/*** 
		BORWSER LOCAL STORAGE ACCESSORS 
		***/

	var storagekey = 'nmeyer-demo-v6';
	var storageitems = [];

	namespace.getItem = function (id) {
		var arr = namespace.getItems().filter(function (obj) {
			return obj['id'] === id;
		});
		return arr[0] || {}; 
	}

	namespace.getItems = function () {
		return storageGet();
	}

	namespace.updateItem = function (obj) {
		var item = mergeEdits(obj); 
		namespace.removeItem(item.id);
		namespace.addItem(item);
	}

	namespace.addItem = function (obj) {
		var items = namespace.getItems(); items.push(obj);
		storageSet(items);
	}

	namespace.removeItem = function (id) {
		var items = namespace.getItems();
		var i = items.map(function (obj) { return obj['id'] }).indexOf(id);

		if (i > -1) {
			items.splice(i, 1);
			storageSet(items);
		}
	}

	namespace.cleanStorage = function () {
		var items = namespace.getItems();
		items = items.filter(function (obj) {
			return !obj['is_deleted'];
		});
		storageSet(items);

	}



	/***
		PRIVATE ROUTINES 
		***/
	function storageSet(storageitems) {
		var str = myApp.utils.jsonEncode(storageitems);
		localStorage.setItem(storagekey, str);
	}

	function storageGet() {
		var str = localStorage.getItem(storagekey);
		return myApp.utils.jsonDecode(str) || [];
	}

	function mergeEdits(obj) {
		var a = namespace.getItem(obj['id']);
		var b = obj || {};

		return $.extend({}, a, b, {  // merge old, new & preserve id
			id: a['id'] || myApp.utils.getGuid(),
			created: a['created'] || Date.now()
		});

	}



})(window, namespace);