﻿'use strict';


var myApp = myApp || {};
var namespace = myApp.utils = myApp.utils || {};




(function (window, namespace) {


	/*** 
		 UTILS 
		 ***/

	namespace.jsonEncode = function (obj) {
		return JSON.stringify(obj);
	}

	namespace.jsonDecode = function (obj) {
		return JSON.parse(obj);
	}

	namespace.getGuid = function () {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	};





})(window, namespace);


