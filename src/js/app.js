'use strict';


var myApp = myApp || {};
var namespace = myApp.presentation = myApp.presentation || {};




(function (window, namespace) {

	var $itemForm = $('form.todo-form');
	var $itemsContainer = $('ul.item-list');


	namespace.init = function () {

		myApp.storage.cleanStorage();

		$itemsContainer.on('click', '.action.remove', namespace.removeItem);
		$itemsContainer.on('click', '.action.edit', namespace.editItem);
		$itemForm.on('click', 'button.submit', namespace.submitForm);
		$itemForm.on('click', 'button.toggle', namespace.toggleForm, true);
		$('button.toggle').click(namespace.toggleForm);

		namespace.updateDisplay();

	}


	/***
		PRESENTATION ROUTINES 
		***/

	namespace.updateDisplay = function () {

		var items = myApp.storage.getItems();
		var $items = $('li', $itemsContainer);

		namespace.updateCounts();

		$.each(items, function (a, b) {
			var $this = $items.filter('[data-id=' + b.id + ']');

			if ($this.length) {
				namespace.removeItem($this)
			}
			namespace.insertItem(b)
		});

	}

	namespace.updateCounts = function () {
		var items = myApp.storage.getItems();

		$('span.items-count')[0].innerHTML = items.length;
		$('span.completed-count')[0].innerHTML = items.filter(function (obj) { return obj['is_completed'] }).length;
		$('span.deleted-count')[0].innerHTML = items.filter(function (obj) { return obj['is_deleted'] }).length;
	}

	namespace.submitForm = function () {
		var obj = {
			id: $('input[name=id]', $itemForm)[0].value,
			title: $('input[name=title]', $itemForm)[0].value,
			is_completed: $('input[name=is_completed]', $itemForm)[0].checked
		};

		myApp.storage.updateItem(obj);
		namespace.toggleForm();
		namespace.updateDisplay();

		return false;
	};


	namespace.editItem = function (obj) {
		var $this = $(obj.currentTarget), $parent = $this.parents('li'),
			item = myApp.storage.getItem($parent.data('id'));

		$.each(item, function (k, v) {
			var $input = $('input[name=' + k + ']', $itemForm);
			if ($input.length) {
				if ($input[0].type === 'checkbox') {
					if (v) $input.attr('checked', v)
					else $input.removeAttr('checked')
				}
				else {
					$input.val(item[k]);
				}
			}
		});
		namespace.toggleForm(true);
	}


	namespace.removeItem = function (obj) {
		var $this = $(obj.currentTarget), $parent = $this.parents('li'),
			item = myApp.storage.getItem($parent.data('id'));
		item['is_deleted'] = true;

		myApp.storage.updateItem(item);
		namespace.updateCounts();
		$parent.addClass('deleted');

	}


	namespace.insertItem = function (obj) {
		var str = '<li data-id="' + obj.id + '">'
			+ '<h1>&ldquo;' + obj.title + '&ldquo; <abbr title="Remove" class="action remove">x</abbr> <abbr title="Edit" class="action edit">e</abbr></h1>'
			+ '<p>&mdash;' + (obj.is_completed ? 'Completed' : 'Not completed') + '</p>'
			+ '</li>';
		$itemsContainer.append(str);
	}


	namespace.toggleForm = function (forceOpen) {
		var state = forceOpen === true || !$('body').hasClass('smr-open');
		$('body').toggleClass('smr-open menu-open', state);
	}




	namespace.init();

})(window, namespace);


